import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  productos: Array<Object> = [];
  constructor (private httpService:HttpService){}

  ngOnInit(){
    this.httpService.getProductos().subscribe((datos)=>{
     this.productos = datos;
    });
    this.httpService.addProducto().subscribe();
    //this.httpService.updateProducto().subscribe();
    //this.httpService.updateProducto().subscribe();
  }
}
