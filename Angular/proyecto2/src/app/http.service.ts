import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class HttpService {
  url= 'https://jquery-lista-compra.firebaseio.com/productos';
  constructor(private http:Http) { }
  getProductos(){
    return this.http.get(this.url + '.json').map((datos) =>{
        let productos = datos.json();
        return Object.values(productos);
    });

  }

  addProducto(producto={nombre:'ALCATEL',precio:1}){
    return this.http.post(this.url+'.json', producto).map(datos=>{
      console.log(datos);
      return datos.json()});
  }

   updateProducto(producto={nombre:'ALCATEL_CAMBIADO',precio:111}){
    return this.http.put(this.url+'/.json', producto).map(datos=>{
      return datos.json()});
  }
   
   deleteProducto(){
    return this.http.delete(this.url+'/.json').map(datos=>{
      return datos.json()});
  }


}
