import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mi-primer-componente',
  templateUrl: './mi-primer-componente.component.html',
  styleUrls: ['./mi-primer-componente.component.css']
})
export class MiPrimerComponenteComponent implements OnInit {

  @Output() emitirNombre= new EventEmitter<String>();
  datosCorrectos= true;
  @Input() numFavorito=5;

  usuario ={
    nombre:'Robb',
    apellido:'Stark'
  };

  constructor() { }

  ngOnInit() {
  }

  getStyles(){
    return{
      color:'red',
      'text-decoration':'line-through'
    };
  }

  getStyles2(){
    return{
      color:'blue',
      'text-decoration':'underline'
    };
  }

  saludar(){
       alert('hola'); 
  }

  enviarNombre(){
    this.emitirNombre.emit("Eduardo");
  }
}
