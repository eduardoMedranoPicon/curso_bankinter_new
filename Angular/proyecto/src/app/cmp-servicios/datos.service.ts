import {Injectable,EventEmitter} from "@angular/core";
import {LogService} from './log.service';

@Injectable ()
export class DatosService {


  private lista = ['Dato1'];
  constructor(private logService:LogService) { }
  emitirDato= new EventEmitter<string>();

  addDatos(dato:string){
    this.logService.escribirConsola('se ha añadido: ' + dato);
    this.lista.push(dato);
  }

  getDatos (){
    return this.lista;
  }

  emitir(dato){
    this.emitirDato.emit(dato);
  }
}
