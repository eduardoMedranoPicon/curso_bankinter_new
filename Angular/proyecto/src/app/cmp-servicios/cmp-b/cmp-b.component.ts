import { Component, OnInit } from '@angular/core';
import { DatosService } from './../datos.service';

@Component({
  selector: 'app-cmp-b',
  templateUrl: './cmp-b.component.html',
  styles: []
})
export class CmpBComponent implements OnInit {

  listaItems= [];
  mensaje='';
  constructor(private datosServices:DatosService) { }

  ngOnInit() {
    this.listaItems = this.datosServices.getDatos();
    this.datosServices.emitirDato.subscribe((dato)=>{
      this.mensaje=dato;
    });
  }

  guardarDato(dato:string){
    this.datosServices.addDatos(dato);
  }
}
