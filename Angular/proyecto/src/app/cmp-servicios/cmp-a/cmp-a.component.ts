import { Component, OnInit } from '@angular/core';
import { DatosService } from './../datos.service';

@Component({
  selector: 'app-cmp-a',
  templateUrl: './cmp-a.component.html',
  styles: [],
  

})
export class CmpAComponent implements OnInit {

  listaItems= [];
  constructor(private datosServices:DatosService) { }

  ngOnInit() {
    this.listaItems = this.datosServices.getDatos();
  }

  guardarDato(dato:string){
    this.datosServices.addDatos(dato);
  }

  enviarDato(dato:string){
    this.datosServices.emitir(dato);
  }


}
