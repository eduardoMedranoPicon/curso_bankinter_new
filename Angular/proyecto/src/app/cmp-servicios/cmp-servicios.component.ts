import { Component, OnInit } from '@angular/core';
import {LogService} from './log.service';
import { DatosService } from './datos.service';
@Component({
  selector: 'app-cmp-servicios',
  templateUrl: './cmp-servicios.component.html',
  styleUrls: ['./cmp-servicios.component.css'],
  providers:[LogService]
})
export class CmpServiciosComponent implements OnInit {

  constructor(private logService:LogService) { }

  ngOnInit() {
  }

  enviarMensaje(){
    this.logService.escribirConsola('Hola a todos');
  }

}
