import { Directive, ElementRef, HostListener , Input, HostBinding} from '@angular/core';

@Directive({
  selector: '[appMarcado]'
})
export class MarcadoDirective {

  @Input('appMarcado') colorMarcado ='yellow';
  private colorinicial ='white';
  @HostBinding('style.backgroundColor')colorFondo:string;

  constructor(private elementRef:ElementRef) { }
  

  @HostListener('mouseenter') onMouseEnter(){
    this.colorFondo= this.colorMarcado;
  }
  
  @HostListener('mouseout') onMouseOut(){
    this.colorFondo= this.colorinicial;
  }


//  @HostListener('mouseenter') onMouseEnter(){
//    this.marcar(this.colorMarcado);
//  }
  
//  @HostListener('mouseout') onMouseOut(){
//    this.marcar(this.colorinicial);
//  }

//  private marcar(color:string){
//    this.elementRef.nativeElement.style.backgroundColor = color;
//  } 
}
