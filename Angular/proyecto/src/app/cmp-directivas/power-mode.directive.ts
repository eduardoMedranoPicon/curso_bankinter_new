import { Directive, HostListener , HostBinding } from '@angular/core';

@Directive({
  selector: '[appPowerMode]'
})
export class PowerModeDirective {


  @HostBinding('style.backgroundColor')colorFondo:string;
  @HostBinding('style.color')colorTexto:string;
  @HostBinding('style.marginTop')marginTop:string='0px';
  @HostBinding('style.marginLeft')marginLeft:string='0px';
  constructor() { }

  @HostListener('input') onInput(){
    this.colorFondo= this.getRandomColor();
    this.colorTexto= this.getRandomColor();
    this.marginTop= this.getRandomMargin() +'px';
    this.marginLeft= this.getRandomMargin() +'px';
    setTimeout(()=>{
      this.marginTop= '0px';
      this.marginLeft= '0px';
      this.colorFondo= 'white';
      this.colorTexto= 'white';
    },150);
  }
  
  private getRandomColor() {
    return  '#' + (Math.random() * 0xFFFFFF << 0).toString(16); 
  }

    private getRandomMargin() {
    return  Math.floor(Math.random()*10); 
  }

}
