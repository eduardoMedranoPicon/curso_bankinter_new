import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cmp-directivas',
  templateUrl: './cmp-directivas.component.html',
  styleUrls: ['./cmp-directivas.component.css']
})
export class CmpDirectivasComponent implements OnInit {

  mascotas=['perro','gato','hamster']
  seMuestra= true;
  miMascota='gato';
  constructor() { }

  ngOnInit() {
  }

  cambiarMuestra(){
    this.seMuestra = !this.seMuestra;
  }

}
