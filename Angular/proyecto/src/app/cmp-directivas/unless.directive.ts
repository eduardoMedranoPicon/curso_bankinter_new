import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  constructor(private templateRef:TemplateRef<any>, private viewContainerRef:ViewContainerRef) {}

    @Input() set appUnless(condicion:boolean){
      if (!condicion){
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      }else{
        this.viewContainerRef.clear();
      }
    }
   

}
