import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-otro-componente',
  template: `
    <p>
      otro-componente works! {{nombre}}
    </p>
  `,
  styles: [`
            p{
              color:blue;
            }
          `]
})
export class OtroComponenteComponent implements OnInit {

  @Input() nombre='';

  constructor() { }

  ngOnInit() {
  }

}
