import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators, FormArray} from '@angular/forms';


@Component({
  selector: 'app-form-datos',
  templateUrl: './form-datos.component.html',
  styleUrls: ['./form-datos.component.css']
})
export class FormDatosComponent implements OnInit {
  
  formulario:FormGroup;
  constructor() { }

  ngOnInit() {
    this.formulario= new FormGroup({
      datosUsuario: new FormGroup({
      nombre: new FormControl('',Validators.required),
      email: new FormControl('', [Validators.required,Validators.pattern("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}")])
      }),
      password: new FormControl('', Validators.required),
      hobbies:new FormArray([
        new FormControl('Tenis',Validators.required),
        new FormControl('Ping-Pong',Validators.required)
      ])
    });
  }

  addHobby(){
    (<FormArray>this.formulario.controls.hobbies).push(new FormControl('',Validators.required));
  }

  onSubmit(){
    console.log(this.formulario);
  }

}
