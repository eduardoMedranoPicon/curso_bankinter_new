import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ErrorComponent } from "./error.component";
import { USUARIO_ROUTES } from "./usuario/usuario.routing";
import { CmpAux1Component } from "./cmp-aux1.component";
import { CmpAux3Component } from "./cmp-aux3.component";
import { CmpAux2Component } from "./cmp-aux2.component";

const APP_ROUTES: Routes = [
    {path:'' , redirectTo: 'inicio',pathMatch:'full'}, //Solo hace la redireccion cuando entremos en root localhost:4200/
    {path:'usuario/:id' , component:UsuarioComponent, children:USUARIO_ROUTES},//Rutas hijas del usuario.routing.ts
    {path:'inicio' , component:InicioComponent},
    //RUTAS UAXILIARES
    {path:'aux1' , component:CmpAux1Component, outlet:'reg-aux1'},
    {path:'aux1' , component:CmpAux3Component, outlet:'reg-aux2'},
    {path:'aux2' , component:CmpAux2Component, outlet:'reg-aux1'},
    {path:'**', component:ErrorComponent}//Ruta comidin
];

export const routing = RouterModule.forRoot(APP_ROUTES);