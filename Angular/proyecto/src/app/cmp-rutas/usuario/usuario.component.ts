import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: []
})
export class UsuarioComponent implements OnInit {

  idUsuario ='';
  visible = true;
  constructor(private router:Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    //De la URL dame el parametro id de usuario
    //this.idUsuario = this.activatedRoute.snapshot.params['id'];

    //Cada vez que cambie el parametro modifica el valor
    this.activatedRoute.params.subscribe((params) =>
    this.idUsuario = params['id']);
    this.activatedRoute.queryParams.subscribe((qParams) =>
    this.visible =qParams['visible']);
  }

  irInicio(){
    this.router.navigate(['']);
  }
  irEditar(){
    this.router.navigate(['usuario',this.idUsuario,'editar'], {queryParams:{datosguardados:true}});
  }

}
