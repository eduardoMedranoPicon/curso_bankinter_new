import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ComponentCanDeactivate } from "./usuario-editar.guard";
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-usuario-editar',
  templateUrl: './usuario-editar.component.html',
  styles: []
})
export class UsuarioEditarComponent implements OnInit,ComponentCanDeactivate {
  datosGuardados = false;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((qParams)=> this.datosGuardados = qParams['datosguardados'] == 'true')
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean{
    if(!this.datosGuardados){
      return confirm('Los datos no se han guardado, si sales los perderas,¿Quieres salir?');
    }
    return true;
  }

}
