import { Routes } from '@angular/router';
import { UsuarioEditarComponent } from "./usuario-editar/usuario-editar.component";
import { UsuarioInfoComponent } from "./usuario-info/usuario-info.component";
import { UsuarioInfoGuard } from "./usuario-info/usuario-info.guard";
import { UsuarioEditarGuard } from "./usuario-editar/usuario-editar.guard";


export const USUARIO_ROUTES: Routes = [
    {path:'info' , component:UsuarioInfoComponent, canActivate:[UsuarioInfoGuard]},
    {path:'editar' , component:UsuarioEditarComponent, canDeactivate:[UsuarioEditarGuard]}

];
