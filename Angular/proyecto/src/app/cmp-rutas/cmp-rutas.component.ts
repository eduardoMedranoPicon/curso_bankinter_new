import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cmp-rutas',
  templateUrl: './cmp-rutas.component.html',
  styleUrls: ['./cmp-rutas.component.css']
})
export class CmpRutasComponent implements OnInit {

  id = '';
  constructor() { }

  ngOnInit() {
  }

  guardarId(id){
    this.id = id;
  }

}
