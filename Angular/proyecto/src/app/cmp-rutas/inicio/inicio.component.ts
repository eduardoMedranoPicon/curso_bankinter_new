import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/interval';
import { Subscription } from "rxjs/Subscription";
import { Observer } from "rxjs/Observer";

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: []
})
export class InicioComponent implements OnInit, OnDestroy {
  subscription:Subscription;
  subscription2:Subscription;
  constructor() { }

  ngOnInit() {
    const nums= Observable.interval(1000);
    this.subscription =nums.subscribe((datos) =>{
      console.log(datos);
    })

    const miObservable = Observable.create((observer:Observer<String>) =>{
      setTimeout(() =>{
        observer.next('Un dato');
      },1000);
      setTimeout(() =>{
        observer.error('Un error');
      },3000);
      setTimeout(() =>{
       observer.complete();
      },6000);

    })

    this.subscription2 =miObservable.subscribe((datos) => console.log(datos));
  
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
     this.subscription2.unsubscribe();
  }


}
