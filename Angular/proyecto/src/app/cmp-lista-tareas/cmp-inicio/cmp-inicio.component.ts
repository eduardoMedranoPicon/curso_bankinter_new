import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { DatosListaService } from './../datosLista.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cmp-inicio',
  templateUrl: './cmp-inicio.component.html',
  styleUrls: ['./cmp-inicio.component.css']
})
export class CmpInicioComponent implements OnInit {
  
  listaItems= [];
  constructor(private datosServices:DatosListaService, private router:Router) { }

  ngOnInit() {
    this.listaItems = this.datosServices.getDatos();
  }

  getTachadoStyle(){
    return{
      color:'red',
      'text-decoration':'line-through'
    };
  }

  irFormulario(){
    this.router.navigate(['formListaTareas']);
  }

  eliminarTareaindex(index:string){
    console.log(index);
    this.datosServices.deleteDatos(index);
  }
}
