import { Component, OnInit } from '@angular/core';
import { DatosListaService } from './../datosLista.service';
import {FormControl, FormGroup, Validators, FormArray} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cmp-form',
  templateUrl: './cmp-form.component.html',
  styleUrls: ['./cmp-form.component.css']
})
export class CmpFormComponent implements OnInit {

  listaItems= [];
  formulario:FormGroup;
  constructor(private datosListaServices:DatosListaService, private router:Router) { }

  ngOnInit() {
    this.formulario= new FormGroup({
      tarea: new FormControl('', Validators.required)
    });
  }

  onSubmit(){
    console.log(this.formulario);
    this.datosListaServices.addDatos(this.formulario.controls.tarea.value);
    this.router.navigate(['inicioListaTareas']);
  }


  cancelarTarea(){
    this.router.navigate(['inicioListaTareas']);
  }

}
