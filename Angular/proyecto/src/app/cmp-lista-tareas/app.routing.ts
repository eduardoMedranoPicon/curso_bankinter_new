import { Routes, RouterModule } from '@angular/router';
import { CmpInicioComponent } from './cmp-inicio/cmp-inicio.component';
import { CmpFormComponent } from './cmp-form/cmp-form.component';
import { ErrorListaTareasComponent } from "./errorListaTareas.component";


const APP_ROUTES_TAREA: Routes = [
    {path:'' , redirectTo: 'inicioListaTareas',pathMatch:'full'}, //Solo hace la redireccion cuando entremos en root localhost:4200/
    {path:'inicioListaTareas' , component:CmpInicioComponent},
    {path:'formListaTareas' , component:CmpFormComponent},
    {path:'**', component:ErrorListaTareasComponent}//Ruta comidin
];

export const routingTareas = RouterModule.forRoot(APP_ROUTES_TAREA);