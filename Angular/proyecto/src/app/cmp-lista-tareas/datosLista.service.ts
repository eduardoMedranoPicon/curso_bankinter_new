import {Injectable,EventEmitter} from "@angular/core";

@Injectable ()
export class DatosListaService {


  private lista = ['Datos1'];
  emitirDato= new EventEmitter<string>();

  addDatos(dato:string){
    this.lista.push(dato);
  }

  getDatos (){
    return this.lista;
  }

  deleteDatos(index:string){
    console.log(index);
    this.lista.splice(parseInt(index),1)
  }

  editDatos (dato:string, index:string){
    this.deleteDatos (index);
    this.addDatos(dato);
  }
}
