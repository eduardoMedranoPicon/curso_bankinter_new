import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cmp-pipes',
  templateUrl: './cmp-pipes.component.html',
  styleUrls: ['./cmp-pipes.component.css']
})
export class CmpPipesComponent implements OnInit {

  texto='Un texto...';
  num=22;
  fecha= new Date(2000,10,2);
  starks=['Arya','Sansa','Rickon','Ned']

  mensaje = new Promise<string>(resolve =>{
    setTimeout(()=>{
      resolve('Un mensaje...');
    },2000);
  });

  guardar(item){
    this.starks.push(item);
  }

  constructor() { }

  ngOnInit() {
  }

}
