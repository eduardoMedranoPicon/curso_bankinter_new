import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'doble'
})
export class DoblePipe implements PipeTransform {

  transform(value: any, arg: any=0, arg2:any=0): any {
    return value*2 + arg -arg2;
  }

}
