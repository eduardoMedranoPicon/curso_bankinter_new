import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sustitute'
})
export class SustitutePipe implements PipeTransform {

  transform(value: any, arg:string[]): any {
    arg.forEach(function(element) {
      value= value.replace(new RegExp(element, 'g'), '*');
    });
    return value;
  }

}
