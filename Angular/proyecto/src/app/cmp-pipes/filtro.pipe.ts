import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
  pure:false
})
export class FiltroPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let resultado = [];
    for(let val of value){
      if(val.match('^.*'+args+'.*$')){
        resultado.push(val);
      }
    }
    return resultado;
  }

}
