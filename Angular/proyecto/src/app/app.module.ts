import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MiPrimerComponenteComponent } from './mi-primer-componente/mi-primer-componente.component';
import { OtroComponenteComponent } from './otro-componente.component';
import { FormsModule } from '@angular/forms';
import { CmpPipesComponent } from './cmp-pipes/cmp-pipes.component';
import { DoblePipe } from './cmp-pipes/doble.pipe';
import { ReversePipe } from './cmp-pipes/reverse.pipe';
import { SustitutePipe } from './cmp-pipes/sustitute.pipe';
import { FiltroPipe } from './cmp-pipes/filtro.pipe';
import { CmpDirectivasComponent } from './cmp-directivas/cmp-directivas.component';
import { MarcadoDirective } from './cmp-directivas/marcado.directive';
import { PowerModeDirective } from './cmp-directivas/power-mode.directive';
import { UnlessDirective } from './cmp-directivas/unless.directive';
import { CmpServiciosComponent } from './cmp-servicios/cmp-servicios.component';
import { CmpAComponent } from './cmp-servicios/cmp-a/cmp-a.component';
import { CmpBComponent } from './cmp-servicios/cmp-b/cmp-b.component';
import { DatosService } from "./cmp-servicios/datos.service";
import { LogService } from "./cmp-servicios/log.service";
import { CmpFormsComponent } from './cmp-forms/cmp-forms.component';
import { FormPlantillaComponent } from './cmp-forms/form-plantilla/form-plantilla.component';
import { FormDatosComponent } from './cmp-forms/form-datos/form-datos.component';
import {FormControl} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { CmpRutasComponent } from './cmp-rutas/cmp-rutas.component';
import { InicioComponent } from './cmp-rutas/inicio/inicio.component';
import { UsuarioComponent } from './cmp-rutas/usuario/usuario.component';
import { routingTareas } from './cmp-lista-tareas/app.routing';
import { routing } from './cmp-rutas/app.routing';
import { ErrorComponent } from './cmp-rutas/error.component';
import { ErrorListaTareasComponent } from './cmp-lista-tareas/errorListaTareas.component';
import { UsuarioEditarComponent } from './cmp-rutas/usuario/usuario-editar/usuario-editar.component';
import { UsuarioInfoComponent } from './cmp-rutas/usuario/usuario-info/usuario-info.component';
import { UsuarioInfoGuard } from "./cmp-rutas/usuario/usuario-info/usuario-info.guard";
import { UsuarioEditarGuard } from "./cmp-rutas/usuario/usuario-editar/usuario-editar.guard";
import { CmpAux1Component } from './cmp-rutas/cmp-aux1.component';
import { CmpAux2Component } from './cmp-rutas/cmp-aux2.component';
import { CmpAux3Component } from './cmp-rutas/cmp-aux3.component';
import { CmpListaTareasComponent } from './cmp-lista-tareas/cmp-lista-tareas.component';
import { CmpInicioComponent } from './cmp-lista-tareas/cmp-inicio/cmp-inicio.component';
import { CmpFormComponent } from './cmp-lista-tareas/cmp-form/cmp-form.component';
import { DatosListaService } from "./cmp-lista-tareas/datosLista.service";

@NgModule({
  declarations: [
    AppComponent,
    MiPrimerComponenteComponent,
    OtroComponenteComponent,
    CmpPipesComponent,
    DoblePipe,
    ReversePipe,
    SustitutePipe,
    FiltroPipe,
    CmpDirectivasComponent,
    MarcadoDirective,
    PowerModeDirective,
    UnlessDirective,
    CmpServiciosComponent,
    CmpAComponent,
    CmpBComponent,
    CmpFormsComponent,
    FormPlantillaComponent,
    FormDatosComponent,
    CmpRutasComponent,
    InicioComponent,
    UsuarioComponent,
    ErrorComponent,
    ErrorListaTareasComponent,
    UsuarioEditarComponent,
    UsuarioInfoComponent,
    CmpAux1Component,
    CmpAux2Component,
    CmpAux3Component,
    CmpListaTareasComponent,
    CmpInicioComponent,
    CmpFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    //routing
    routingTareas
  ],
  providers: [DatosService, DatosListaService, LogService, UsuarioInfoGuard, UsuarioEditarGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
