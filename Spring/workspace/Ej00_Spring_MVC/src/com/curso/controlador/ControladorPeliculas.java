package com.curso.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.negocio.GestorPeliculas;

@Controller
public class ControladorPeliculas {
		@Autowired
		private GestorPeliculas gestorPeliculas;
		
		@RequestMapping("insertarPelicula")
		public ModelAndView insertar(@ModelAttribute("pelicula") Pelicula pelicula) {
			gestorPeliculas.insertar(pelicula);
			return null;
		}
		
		@RequestMapping("borrarPelicula")
		public ModelAndView borrar(@RequestParam("id") Integer id) {
			Pelicula pelicula = new Pelicula();
			pelicula.setId(id);
			gestorPeliculas.borrar(pelicula );
			return null;
		}
		
		@RequestMapping("verListadoPeliculas")
		public ModelAndView mostrarListado() {

			ModelAndView mav =  new ModelAndView("listadoPeliculas");
			mav.addObject("peliculas",gestorPeliculas.listar());
			return mav;
		}
		
		@RequestMapping("verFormularioPeliculas")
		public ModelAndView mostrarFormulario() {
			ModelAndView mav =  new ModelAndView("formularioPeliculas");
			mav.addObject("pelicula",new Pelicula());
			return mav;
		}
}
