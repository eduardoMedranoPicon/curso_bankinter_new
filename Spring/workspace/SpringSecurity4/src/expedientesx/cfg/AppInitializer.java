package expedientesx.cfg;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

public class AppInitializer implements WebApplicationInitializer {
	
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
    	
    	WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/expedientesx/*");
        
        servletContext.addFilter("springSecurityFilterChain", 
        		new DelegatingFilterProxy("springSecurityFilterChain"))
        			.addMappingForUrlPatterns(null, false, "/*");  
        
//       Es equivalente a poner esto en el web.xml        
//        <!-- Avisa cuando la aplicacion se despliega-->
//        <listener>
//          <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
//        </listener>
//        
//        <servlet>
//          <servlet-name>spring-mvc</servlet-name>
//          <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
//          <init-param>
//            <param-name>contextConfigLocation</param-name>
//            <param-value>/WEB-INF/spring-mvc.xml</param-value>
//          </init-param>
//          <load-on-startup>1</load-on-startup>
//        </servlet>
        
        servletContext.addListener(new HttpSessionEventPublisher());
       
    }
    

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("expedientesx.cfg");
        return context;
    }
}