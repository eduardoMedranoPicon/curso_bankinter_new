import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.modelo.negocio.GestorPeliculas;

public class Pruebas {
	
	public static void main(String[] args) {
	
		ApplicationContext appçctx = new ClassPathXmlApplicationContext("com.curso.cfg.Beans.xml");
		
		//Por nombre
		GestorPeliculas gp = (GestorPeliculas) appçctx.getBean("gestorPeliculas");
		
		//Por tipo
		GestorPeliculas gp2 = (GestorPeliculas) appçctx.getBean(GestorPeliculas.class);
		
	}

}
