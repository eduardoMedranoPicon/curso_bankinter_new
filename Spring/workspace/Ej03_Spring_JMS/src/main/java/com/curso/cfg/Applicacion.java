package com.curso.cfg;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.swing.JOptionPane;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

//activemq start
//http://127.0.0.1:8161/admin/
//admin
//admin
public class Applicacion {

	public static void main(String[] args) {
		
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(Configuracion.class);		
		Emisor e = (Emisor) appCtx.getBean("emisor");
		e.send("cola1", "Hola Radiola");	
		e.send("cola2", "Siete caballos vienen de Bonanza");	
		
		
        MessageCreator messageCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("Zas!");
            }
        };		
        JmsTemplate jmsTemplate = appCtx.getBean(JmsTemplate.class);
        System.out.println("Enviando mensajes");
        jmsTemplate.send("cola1", messageCreator);
        jmsTemplate.send("cola2", messageCreator);		
       
        
		JOptionPane.showMessageDialog(null, "Hola");
		
	}
	
}
