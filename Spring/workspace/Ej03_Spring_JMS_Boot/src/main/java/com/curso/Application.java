package com.curso;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

@SpringBootApplication
@EnableJms
public class Application {
	
    public static void main(String[] args) {
        //Eliminamos cualquier mensaje existente
        //FileSystemUtils.deleteRecursively(new File("activemq-data"));

        ConfigurableApplicationContext appCtx = SpringApplication.run(Application.class, args);

		Emisor e = (Emisor) appCtx.getBean("emisor");
		e.send("cola1", "Hola Radiola");	
		e.send("cola2", "Siete caballos vienen de Bonanza");	        
        
        
        MessageCreator messageCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("Zas!");
            }
        };      
        JmsTemplate jmsTemplate = appCtx.getBean(JmsTemplate.class);
        System.out.println("Enviando mensajes");
        jmsTemplate.send("cola1", messageCreator);
        jmsTemplate.send("cola2", messageCreator);
    }

}