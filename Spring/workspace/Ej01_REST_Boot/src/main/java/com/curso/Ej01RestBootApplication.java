package com.curso;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.DelegatingFilterProxy;

//import de.codecentric.boot.admin.config.EnableAdminServer;


@SpringBootApplication
//@EnableAdminServer
public class Ej01RestBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ej01RestBootApplication.class, args);
	}
	
	
}
