package com.jersey.spring.cliente;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jersey.spring.JerseyApplication;
import com.jersey.spring.service.rest.HolaMundoRestService;

public class AplicacionCliente {
	
	public static void main (String[] args) {
		
		//GET
		Client client = ClientBuilder.newClient();
		String saludo=client
		.target("http://localhost:8080/JerseySpring/api/v1/")
		.path("saludos")
		.request(MediaType.APPLICATION_JSON)
		.get(String.class);
		System.out.println(saludo+" ok");
	}

}
