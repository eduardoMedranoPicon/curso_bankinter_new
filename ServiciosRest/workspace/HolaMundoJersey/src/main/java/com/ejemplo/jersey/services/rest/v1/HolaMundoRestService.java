package com.ejemplo.jersey.services.rest.v1;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//Tambien se podria llamar HolaMundoResource
//Siempre sustantivo y en plural
@Path("/saludos")
public class HolaMundoRestService {
	
	private static final String URL_SALUDOS = "http://loclahost:8080/HolaMundoJersey/api/v1/saludos/";
	
	//Describa las operaciones que se pueden realizar sobre el recurso
	
	//Eb este servicio los saludos y prefijos son string
	@GET
	@Produces (MediaType.APPLICATION_XML)// Cabecera Accept
	public Response consultarTodos(){
		System.out.println("------------Inicio consultarTodos-------------");
		System.out.println("------------Fin consultarTodos-------------");
		return Response.ok().build();
	}

	@GET
	@Path("{id}")
	public Response consultarPorId(@PathParam("id") long id){
		return null;
	}
	
	@GET
	@Path("{id}/prefijo")
	public Response consultarPrefijoDeUnSaludoPorId(@PathParam("id") long id){
		return null;
	}
	
	@POST
	@Consumes (MediaType.APPLICATION_XML)
	public Response insertar(String saludo) throws URISyntaxException{
		long idgenerado =1;
		return Response.created(new URI(URL_SALUDOS + idgenerado)).build();
	}
}
