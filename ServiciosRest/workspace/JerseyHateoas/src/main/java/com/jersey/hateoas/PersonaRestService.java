package com.jersey.hateoas;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

@Path("personas")
public class PersonaRestService {
	
	@GET
	@Path("{id})")
	public Persona consulta(String id) {
		return null;
	}

	@PUT
	@Path("{id})")
	public Persona modificacion(String id, Persona persona) {
		return null;
	}
	
	@DELETE
	@Path("{id})")
	public Persona alta(String id) {
		return null;
	}
	
	@GET
	@Path("{id}/hermanos)")
	public Persona consultaHermanos(String id) {
		return null;
	}
}
