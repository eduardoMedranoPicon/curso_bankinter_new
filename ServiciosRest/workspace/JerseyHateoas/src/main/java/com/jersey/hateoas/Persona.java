package com.jersey.hateoas;

import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Persona {
	
	private String id;
	private String nombre;
	private int edad;
	
	@InjectLinks(
			{
			@InjectLink(
						resource = PersonaRestService.class,
						style = Style.ABSOLUTE,
						rel = "modificacion",
						bindings = @Binding(name = "id", value = "${instance.id}"),
						method = "modificacion"),
			@InjectLink(
						resource = PersonaRestService.class,
						style = Style.ABSOLUTE,
						rel = "self",
						bindings = @Binding(name = "id", value = "${instance.id}"),	method = "crear",extensions = {
					 	@Extension(name = "method", value = "POST")})
			})
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "self");
	private List<Link> self;
	
	@InjectLinks(
			{
			@InjectLink(resource = PersonaRestService.class,
					    style = Style.ABSOLUTE,
					    rel = "self",
					    bindings = @Binding(name = "id", value = "${instance.id}"),	method = "consultarHermanos"),
						extensions = {@Extension(name = "method", value = "GET")}
			}
			)
			
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "hermanos");
	private Link hermanos;

}
