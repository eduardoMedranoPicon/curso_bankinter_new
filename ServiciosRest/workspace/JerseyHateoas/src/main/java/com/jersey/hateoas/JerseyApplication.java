package com.jersey.hateoas;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api/v1")
public class JerseyApplication extends ResourceConfig {
	
	public JerseyApplication() {
		super();
		//Definicion del paquete a escanear en busca de clasdes anotadas a escanear
		packages(true, this.getClass().getPackage().getName()+ ".service.rest");

	}

}
