package com.jersey.simpsons.services.rest.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

@XmlRootElement
public class CharacterDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull
	private int id;
	@NotEmpty
	private String name;
	private int age;
	private CharacterDTO parent;
	private CharacterDTO mother;
	private CharacterDTO married;
	private List <CharacterDTO> children;
	private List <CharacterDTO> brother;
	
	public CharacterDTO (int id, String name, int age, CharacterDTO parent, CharacterDTO mother, CharacterDTO married, List<CharacterDTO> children, List<CharacterDTO> brother) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.parent = parent;
		this.mother = mother;
		this.married = married;
		this.children = children;
		this.brother = brother;
		
	}
	
	public CharacterDTO (int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.parent = new CharacterDTO();
		this.mother = new CharacterDTO();
		this.married = new CharacterDTO();
		this.children = new ArrayList<CharacterDTO>();
		this.brother = new ArrayList<CharacterDTO>();
		
	}
	
	public CharacterDTO () {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CharacterDTO getParent() {
		return parent;
	}
	public void setParent(CharacterDTO parent) {
		this.parent = parent;
	}
	public CharacterDTO getMother() {
		return mother;
	}
	public void setMother(CharacterDTO mother) {
		this.mother = mother;
	}
	public CharacterDTO getMarried() {
		return married;
	}
	public void setMarried(CharacterDTO married) {
		this.married = married;
	}
	public List<CharacterDTO> getChildren() {
		return children;
	}

	public void setChildren(List<CharacterDTO> children) {
		this.children = children;
	}

	public List<CharacterDTO> getBrother() {
		return brother;
	}

	public void setBrother(List<CharacterDTO> brother) {
		this.brother = brother;
	}
	

}
