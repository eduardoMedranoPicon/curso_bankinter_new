package com.jersey.simpsons;

import org.glassfish.jersey.internal.inject.AbstractBinder;

import com.jersey.simpsons.core.services.Servicio;
import com.jersey.simpsons.core.services.ServicioImpl;

public class MiContextoBeans extends AbstractBinder {

	public MiContextoBeans() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		bind(ServicioImpl.class).to(Servicio.class);

	}

}
