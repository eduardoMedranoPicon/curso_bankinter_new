package com.jersey.simpsons;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

@ApplicationPath("/api/v1")
public class JerseyApplication extends ResourceConfig {
	
	public JerseyApplication() {
		super();
		//Definicion del paquete a escanear en busca de clasdes anotadas a escanear
		packages(true, this.getClass().getPackage().getName());
		// Con esto conseguiriamos que al enviar un dato validado devuelve el feetback al cliente
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		//Registrar un bean de binding
		//register(new MiContextoBeans());
	}

}
