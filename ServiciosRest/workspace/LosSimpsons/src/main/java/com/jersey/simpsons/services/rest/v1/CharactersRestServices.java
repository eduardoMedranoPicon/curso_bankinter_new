package com.jersey.simpsons.services.rest.v1;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jersey.simpsons.core.services.Servicio;

//Tambien se podria llamar HolaMundoResource
//Siempre sustantivo y en plural
@Path("/characters")
public class CharactersRestServices {
	
//	private List<CharacterDTO> personajes;
		
//    @PostConstruct
//    public void initPersonajes(){
//        System.out.println("------------CREO LOS PERSONAJES-------------");
////    	long id;
////    	String name;
////    	int age;
////    	CharacterDTO parent;
////    	CharacterDTO mother;
////    	CharacterDTO married;
////    	List <CharacterDTO> children;
////      List <CharacterDTO> brother;
//        personajes = new ArrayList<CharacterDTO>();
//        CharacterDTO c1 = new CharacterDTO(1, "Homer", 38);
//        personajes.add(c1);
//        c1 = new CharacterDTO(2, "Marge", 36);
//        personajes.add(c1);
//        c1 = new CharacterDTO(3, "Bart", 10);
//        personajes.add(c1);
//        c1 = new CharacterDTO(4, "Lisa", 8);
//        personajes.add(c1);
//        c1 = new CharacterDTO(5, "Maggie", 1);
//        personajes.add(c1);
//        c1 = new CharacterDTO(6, "Abe", 86);
//        personajes.add(c1);
//        
//        System.out.println("------------PERSONAJES CREADOS-------------");
//    }
		
		//Describa las operaciones que se pueden realizar sobre el recurso
		
		//En este servicio los saludos y prefijos son string
		@Inject
		private Servicio servicio;
	
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<CharacterDTO> consultarTodos(){
			System.out.println("------------Inicio consultarTodos-------------");
			//servicio.funcionalidadDeLaCapaDeNegocio();
			System.out.println("------------Fin consultarTodos-------------");
			return null;
		}

		@GET
		@Path("{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public CharacterDTO consultarPorId(@PathParam("id") String id){
			String [] split = id.split(":");
			if (split.length>1 && split[0].equals("nickname")) {
				String alias = split[1];
			}
				
			
			return new CharacterDTO(1,"Hommer Simpsons",38);
		}
		
		@GET
		@Path("{id}/hermanos")
		public List<CharacterDTO> consultarHermanosPorId(@PathParam("id") int id){
			return null;
		}
		
		@GET
		@Path("{id}/hermanos/{idHermano}")
		public CharacterDTO consultarHermanoPorId(@PathParam("id") int id, @PathParam("idHermano") int idHermano){
			return null;
		}

		//ALTA DEL REGISTRO COMPLETO
		@PUT
		public Response insertar(CharacterDTO personaje){
			return Response.ok().build();
		}
		
		//MODIFICACION PARCIAL DE LAS CARRACTERISTICAS QUE SE RECIBAN
		@PATCH
		@PUT
		@Path("{id}")
		public void modificar(@Valid CharacterDTO personaje,@PathParam("id") int id){

		}
		
		//BORRADO
		@DELETE
		@Path("{id}")
		public void borrar(@PathParam("id") int id){

		}
		
//		
//		
//		@POST
//		@Consumes (MediaType.APPLICATION_XML)
//		public Response insertar(String nombre) throws URISyntaxException{
//			long idgenerado =1;
//			return null;
//		}
}
